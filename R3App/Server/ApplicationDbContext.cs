﻿using BlazorBootstrap;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using R3App.Shared.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Reflection.Metadata;

namespace R3App.Server
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes()
            .SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<EstudianteFamiliar>()
                .HasKey(x => new { x.FamiliarId, x.EstudianteId });

            modelBuilder.Entity<EstudianteCurso>()
                .HasKey(x => new { x.EstudianteId, x.CursoId });

            modelBuilder.Entity<DocenteAsignatura>()
               .HasKey(x => new { x.DocenteId, x.AsignaturaId });

            modelBuilder.Entity<RegistroAsistencia>()
                .HasKey(x => new { x.Fecha,x.CursoId, x.EstudianteId });

            modelBuilder.Entity<AsignaturaCurso>()
                .HasKey(x => new { x.AsignaturaId, x.CursoId });

            modelBuilder.Entity<Horario>().HasKey(x => new { x.Hora, x.Dia });

            modelBuilder.Entity<Curso>()
                .HasMany(curso => curso.Estudiantes)
                .WithOne(estudiante => estudiante.Curso)
                .HasForeignKey(estudiante => estudiante.CursoId);

            modelBuilder.Entity<Estudiante>()
               .HasOne(x => x.Legajo)
               .WithOne(x => x.Estudiante)
               .HasForeignKey<Legajo>(x => x.EstudianteId);

        }

        public DbSet<Estudiante> Estudiantes => Set<Estudiante>();
        public DbSet<Curso> Cursos => Set<Curso>();
        public DbSet<Turno> Turnos => Set<Turno>();
        public DbSet<Docente> Docentes => Set<Docente>();
        public DbSet<Asignatura> Asignaturas => Set<Asignatura>();
        public DbSet<Calificacion> Calificaciones => Set<Calificacion>();
        public DbSet<RegistroAsistencia> RegistroAsistencias => Set<RegistroAsistencia>();
        public DbSet<Area> Areas => Set<Area>();
        public DbSet<Horario> Horarios => Set<Horario>();
        public DbSet<Familiar> Familiares => Set<Familiar>();
        public DbSet<EstudianteFamiliar> EstudianteFamiliares => Set<EstudianteFamiliar>();
        public DbSet<Calendario> Calendarios => Set<Calendario>();
        public DbSet<EstudianteCurso> EstudianteCursos => Set<EstudianteCurso>();
        public DbSet<AsignaturaCurso> AsignaturaCursos => Set<AsignaturaCurso>();



    }
}
