﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class registro_20231031_8 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistroAsistencias_Docentes_DocenteId",
                table: "RegistroAsistencias");

            migrationBuilder.DropIndex(
                name: "IX_RegistroAsistencias_DocenteId",
                table: "RegistroAsistencias");

            migrationBuilder.DropColumn(
                name: "DocenteId",
                table: "RegistroAsistencias");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DocenteId",
                table: "RegistroAsistencias",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RegistroAsistencias_DocenteId",
                table: "RegistroAsistencias",
                column: "DocenteId");

            migrationBuilder.AddForeignKey(
                name: "FK_RegistroAsistencias_Docentes_DocenteId",
                table: "RegistroAsistencias",
                column: "DocenteId",
                principalTable: "Docentes",
                principalColumn: "DocenteId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
