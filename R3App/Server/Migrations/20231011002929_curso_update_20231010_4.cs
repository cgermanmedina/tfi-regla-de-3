﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class curso_update_20231010_4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocenteAsignatura_Asignaturas_AsignaturaId",
                table: "DocenteAsignatura");

            migrationBuilder.DropForeignKey(
                name: "FK_DocenteAsignatura_Docentes_DocenteId",
                table: "DocenteAsignatura");

            migrationBuilder.DropTable(
                name: "AsignaturaDocente");

            migrationBuilder.AddForeignKey(
                name: "FK_DocenteAsignatura_Asignaturas_AsignaturaId",
                table: "DocenteAsignatura",
                column: "AsignaturaId",
                principalTable: "Asignaturas",
                principalColumn: "AsignaturaId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocenteAsignatura_Docentes_DocenteId",
                table: "DocenteAsignatura",
                column: "DocenteId",
                principalTable: "Docentes",
                principalColumn: "DocenteId",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocenteAsignatura_Asignaturas_AsignaturaId",
                table: "DocenteAsignatura");

            migrationBuilder.DropForeignKey(
                name: "FK_DocenteAsignatura_Docentes_DocenteId",
                table: "DocenteAsignatura");

            migrationBuilder.CreateTable(
                name: "AsignaturaDocente",
                columns: table => new
                {
                    AsignaturasAsignaturaId = table.Column<int>(type: "int", nullable: false),
                    DocentesDocenteId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AsignaturaDocente", x => new { x.AsignaturasAsignaturaId, x.DocentesDocenteId });
                    table.ForeignKey(
                        name: "FK_AsignaturaDocente_Asignaturas_AsignaturasAsignaturaId",
                        column: x => x.AsignaturasAsignaturaId,
                        principalTable: "Asignaturas",
                        principalColumn: "AsignaturaId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AsignaturaDocente_Docentes_DocentesDocenteId",
                        column: x => x.DocentesDocenteId,
                        principalTable: "Docentes",
                        principalColumn: "DocenteId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AsignaturaDocente_DocentesDocenteId",
                table: "AsignaturaDocente",
                column: "DocentesDocenteId");

            migrationBuilder.AddForeignKey(
                name: "FK_DocenteAsignatura_Asignaturas_AsignaturaId",
                table: "DocenteAsignatura",
                column: "AsignaturaId",
                principalTable: "Asignaturas",
                principalColumn: "AsignaturaId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocenteAsignatura_Docentes_DocenteId",
                table: "DocenteAsignatura",
                column: "DocenteId",
                principalTable: "Docentes",
                principalColumn: "DocenteId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
