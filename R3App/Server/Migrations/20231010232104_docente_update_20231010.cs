﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class docente_update_20231010 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Curso",
                table: "Asignaturas",
                newName: "AnioCurso");

            migrationBuilder.AlterColumn<int>(
                name: "CursoId",
                table: "Asignaturas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AnioCurso",
                table: "Asignaturas",
                newName: "Curso");

            migrationBuilder.AlterColumn<int>(
                name: "CursoId",
                table: "Asignaturas",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
