﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class registro_20231031_7 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistroAsistencias_Asignaturas_AsignaturaId",
                table: "RegistroAsistencias");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.DropIndex(
                name: "IX_RegistroAsistencias_AsignaturaId",
                table: "RegistroAsistencias");

            migrationBuilder.DropColumn(
                name: "AsignaturaId",
                table: "RegistroAsistencias");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.AddColumn<int>(
                name: "AsignaturaId",
                table: "RegistroAsistencias",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId", "AsignaturaId" });

            migrationBuilder.CreateIndex(
                name: "IX_RegistroAsistencias_AsignaturaId",
                table: "RegistroAsistencias",
                column: "AsignaturaId");

            migrationBuilder.AddForeignKey(
                name: "FK_RegistroAsistencias_Asignaturas_AsignaturaId",
                table: "RegistroAsistencias",
                column: "AsignaturaId",
                principalTable: "Asignaturas",
                principalColumn: "AsignaturaId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
