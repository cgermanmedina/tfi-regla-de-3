﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class registro_20231031_3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.DropColumn(
                name: "AsistenciaDiaria",
                table: "RegistroAsistencias");

            migrationBuilder.DropColumn(
                name: "AsistenciaPorAsignatura",
                table: "RegistroAsistencias");

            migrationBuilder.AlterColumn<string>(
                name: "Observacion",
                table: "RegistroAsistencias",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<int>(
                name: "DocenteId",
                table: "RegistroAsistencias",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId", "AsignaturaId" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.AlterColumn<string>(
                name: "Observacion",
                table: "RegistroAsistencias",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DocenteId",
                table: "RegistroAsistencias",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AsistenciaDiaria",
                table: "RegistroAsistencias",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AsistenciaPorAsignatura",
                table: "RegistroAsistencias",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId" });
        }
    }
}
