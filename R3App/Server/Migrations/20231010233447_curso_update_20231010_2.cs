﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class curso_update_20231010_2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Asignaturas_AsignaturaId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Cursos_CursoId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Docentes_DocenteId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_Asignaturas_Cursos_CursoId",
                table: "Asignaturas");

            migrationBuilder.DropForeignKey(
                name: "FK_Cursos_Estudiantes_DelegadoId",
                table: "Cursos");

            migrationBuilder.DropForeignKey(
                name: "FK_Estudiantes_Cursos_CursoId",
                table: "Estudiantes");

            migrationBuilder.DropForeignKey(
                name: "FK_Horarios_AsignaturaCurso_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios");

            migrationBuilder.DropIndex(
                name: "IX_Asignaturas_CursoId",
                table: "Asignaturas");

            migrationBuilder.DropColumn(
                name: "CursoId",
                table: "Asignaturas");

            migrationBuilder.CreateTable(
                name: "EstudianteCurso",
                columns: table => new
                {
                    EstudianteId = table.Column<int>(type: "int", nullable: false),
                    CursoId = table.Column<int>(type: "int", nullable: false),
                    Observaciones = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AnioCursada = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstudianteCurso", x => new { x.EstudianteId, x.CursoId });
                    table.ForeignKey(
                        name: "FK_EstudianteCurso_Cursos_CursoId",
                        column: x => x.CursoId,
                        principalTable: "Cursos",
                        principalColumn: "CursoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EstudianteCurso_Estudiantes_EstudianteId",
                        column: x => x.EstudianteId,
                        principalTable: "Estudiantes",
                        principalColumn: "EstudianteId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EstudianteCurso_CursoId",
                table: "EstudianteCurso",
                column: "CursoId");

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Asignaturas_AsignaturaId",
                table: "AsignaturaCurso",
                column: "AsignaturaId",
                principalTable: "Asignaturas",
                principalColumn: "AsignaturaId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Cursos_CursoId",
                table: "AsignaturaCurso",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Docentes_DocenteId",
                table: "AsignaturaCurso",
                column: "DocenteId",
                principalTable: "Docentes",
                principalColumn: "DocenteId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cursos_Estudiantes_DelegadoId",
                table: "Cursos",
                column: "DelegadoId",
                principalTable: "Estudiantes",
                principalColumn: "EstudianteId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Estudiantes_Cursos_CursoId",
                table: "Estudiantes",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Horarios_AsignaturaCurso_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios",
                columns: new[] { "AsignaturaCursoAsignaturaId", "AsignaturaCursoCursoId" },
                principalTable: "AsignaturaCurso",
                principalColumns: new[] { "AsignaturaId", "CursoId" },
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Asignaturas_AsignaturaId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Cursos_CursoId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Docentes_DocenteId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_Cursos_Estudiantes_DelegadoId",
                table: "Cursos");

            migrationBuilder.DropForeignKey(
                name: "FK_Estudiantes_Cursos_CursoId",
                table: "Estudiantes");

            migrationBuilder.DropForeignKey(
                name: "FK_Horarios_AsignaturaCurso_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios");

            migrationBuilder.DropTable(
                name: "EstudianteCurso");

            migrationBuilder.AddColumn<int>(
                name: "CursoId",
                table: "Asignaturas",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Asignaturas_CursoId",
                table: "Asignaturas",
                column: "CursoId");

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Asignaturas_AsignaturaId",
                table: "AsignaturaCurso",
                column: "AsignaturaId",
                principalTable: "Asignaturas",
                principalColumn: "AsignaturaId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Cursos_CursoId",
                table: "AsignaturaCurso",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Docentes_DocenteId",
                table: "AsignaturaCurso",
                column: "DocenteId",
                principalTable: "Docentes",
                principalColumn: "DocenteId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Asignaturas_Cursos_CursoId",
                table: "Asignaturas",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cursos_Estudiantes_DelegadoId",
                table: "Cursos",
                column: "DelegadoId",
                principalTable: "Estudiantes",
                principalColumn: "EstudianteId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Estudiantes_Cursos_CursoId",
                table: "Estudiantes",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Horarios_AsignaturaCurso_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios",
                columns: new[] { "AsignaturaCursoAsignaturaId", "AsignaturaCursoCursoId" },
                principalTable: "AsignaturaCurso",
                principalColumns: new[] { "AsignaturaId", "CursoId" });
        }
    }
}
