﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class asignaturacurso2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Asignaturas_AsignaturaId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Cursos_CursoId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCurso_Docentes_DocenteId",
                table: "AsignaturaCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_Horarios_AsignaturaCurso_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AsignaturaCurso",
                table: "AsignaturaCurso");

            migrationBuilder.RenameTable(
                name: "AsignaturaCurso",
                newName: "AsignaturaCursos");

            migrationBuilder.RenameIndex(
                name: "IX_AsignaturaCurso_DocenteId",
                table: "AsignaturaCursos",
                newName: "IX_AsignaturaCursos_DocenteId");

            migrationBuilder.RenameIndex(
                name: "IX_AsignaturaCurso_CursoId",
                table: "AsignaturaCursos",
                newName: "IX_AsignaturaCursos_CursoId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AsignaturaCursos",
                table: "AsignaturaCursos",
                columns: new[] { "AsignaturaId", "CursoId" });

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCursos_Asignaturas_AsignaturaId",
                table: "AsignaturaCursos",
                column: "AsignaturaId",
                principalTable: "Asignaturas",
                principalColumn: "AsignaturaId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCursos_Cursos_CursoId",
                table: "AsignaturaCursos",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCursos_Docentes_DocenteId",
                table: "AsignaturaCursos",
                column: "DocenteId",
                principalTable: "Docentes",
                principalColumn: "DocenteId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Horarios_AsignaturaCursos_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios",
                columns: new[] { "AsignaturaCursoAsignaturaId", "AsignaturaCursoCursoId" },
                principalTable: "AsignaturaCursos",
                principalColumns: new[] { "AsignaturaId", "CursoId" },
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCursos_Asignaturas_AsignaturaId",
                table: "AsignaturaCursos");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCursos_Cursos_CursoId",
                table: "AsignaturaCursos");

            migrationBuilder.DropForeignKey(
                name: "FK_AsignaturaCursos_Docentes_DocenteId",
                table: "AsignaturaCursos");

            migrationBuilder.DropForeignKey(
                name: "FK_Horarios_AsignaturaCursos_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AsignaturaCursos",
                table: "AsignaturaCursos");

            migrationBuilder.RenameTable(
                name: "AsignaturaCursos",
                newName: "AsignaturaCurso");

            migrationBuilder.RenameIndex(
                name: "IX_AsignaturaCursos_DocenteId",
                table: "AsignaturaCurso",
                newName: "IX_AsignaturaCurso_DocenteId");

            migrationBuilder.RenameIndex(
                name: "IX_AsignaturaCursos_CursoId",
                table: "AsignaturaCurso",
                newName: "IX_AsignaturaCurso_CursoId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AsignaturaCurso",
                table: "AsignaturaCurso",
                columns: new[] { "AsignaturaId", "CursoId" });

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Asignaturas_AsignaturaId",
                table: "AsignaturaCurso",
                column: "AsignaturaId",
                principalTable: "Asignaturas",
                principalColumn: "AsignaturaId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Cursos_CursoId",
                table: "AsignaturaCurso",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AsignaturaCurso_Docentes_DocenteId",
                table: "AsignaturaCurso",
                column: "DocenteId",
                principalTable: "Docentes",
                principalColumn: "DocenteId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Horarios_AsignaturaCurso_AsignaturaCursoAsignaturaId_AsignaturaCursoCursoId",
                table: "Horarios",
                columns: new[] { "AsignaturaCursoAsignaturaId", "AsignaturaCursoCursoId" },
                principalTable: "AsignaturaCurso",
                principalColumns: new[] { "AsignaturaId", "CursoId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
