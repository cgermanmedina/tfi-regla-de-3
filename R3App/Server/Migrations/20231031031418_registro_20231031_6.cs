﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class registro_20231031_6 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.AlterColumn<int>(
                name: "AsignaturaId",
                table: "RegistroAsistencias",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId", "AsignaturaId" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.AlterColumn<int>(
                name: "AsignaturaId",
                table: "RegistroAsistencias",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId" });
        }
    }
}
