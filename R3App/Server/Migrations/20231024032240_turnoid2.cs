﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class turnoid2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cursos_Turnos_TuronoId",
                table: "Cursos");

            migrationBuilder.DropIndex(
                name: "IX_Cursos_TuronoId",
                table: "Cursos");

            migrationBuilder.DropColumn(
                name: "TuronoId",
                table: "Cursos");

            migrationBuilder.CreateIndex(
                name: "IX_Cursos_TurnoId",
                table: "Cursos",
                column: "TurnoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cursos_Turnos_TurnoId",
                table: "Cursos",
                column: "TurnoId",
                principalTable: "Turnos",
                principalColumn: "TurnoId",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cursos_Turnos_TurnoId",
                table: "Cursos");

            migrationBuilder.DropIndex(
                name: "IX_Cursos_TurnoId",
                table: "Cursos");

            migrationBuilder.AddColumn<int>(
                name: "TuronoId",
                table: "Cursos",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cursos_TuronoId",
                table: "Cursos",
                column: "TuronoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cursos_Turnos_TuronoId",
                table: "Cursos",
                column: "TuronoId",
                principalTable: "Turnos",
                principalColumn: "TurnoId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
