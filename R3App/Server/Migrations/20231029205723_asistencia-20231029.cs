﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class asistencia20231029 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.AddColumn<int>(
                name: "CursoId",
                table: "RegistroAsistencias",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EstadoAsistencia",
                table: "RegistroAsistencias",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId", "AsignaturaId" });

            migrationBuilder.CreateIndex(
                name: "IX_RegistroAsistencias_CursoId",
                table: "RegistroAsistencias",
                column: "CursoId");

            migrationBuilder.AddForeignKey(
                name: "FK_RegistroAsistencias_Cursos_CursoId",
                table: "RegistroAsistencias",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistroAsistencias_Cursos_CursoId",
                table: "RegistroAsistencias");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.DropIndex(
                name: "IX_RegistroAsistencias_CursoId",
                table: "RegistroAsistencias");

            migrationBuilder.DropColumn(
                name: "CursoId",
                table: "RegistroAsistencias");

            migrationBuilder.DropColumn(
                name: "EstadoAsistencia",
                table: "RegistroAsistencias");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "EstudianteId", "AsignaturaId" });
        }
    }
}
