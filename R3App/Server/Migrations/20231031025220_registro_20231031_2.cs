﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class registro_20231031_2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RegistroAsistencias",
                table: "RegistroAsistencias",
                columns: new[] { "Fecha", "CursoId", "EstudianteId", "AsignaturaId" });
        }
    }
}
