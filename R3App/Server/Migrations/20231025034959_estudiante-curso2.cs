﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class estudiantecurso2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EstudianteCurso_Cursos_CursoId",
                table: "EstudianteCurso");

            migrationBuilder.DropForeignKey(
                name: "FK_EstudianteCurso_Estudiantes_EstudianteId",
                table: "EstudianteCurso");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EstudianteCurso",
                table: "EstudianteCurso");

            migrationBuilder.RenameTable(
                name: "EstudianteCurso",
                newName: "EstudianteCursos");

            migrationBuilder.RenameIndex(
                name: "IX_EstudianteCurso_CursoId",
                table: "EstudianteCursos",
                newName: "IX_EstudianteCursos_CursoId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EstudianteCursos",
                table: "EstudianteCursos",
                columns: new[] { "EstudianteId", "CursoId" });

            migrationBuilder.AddForeignKey(
                name: "FK_EstudianteCursos_Cursos_CursoId",
                table: "EstudianteCursos",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EstudianteCursos_Estudiantes_EstudianteId",
                table: "EstudianteCursos",
                column: "EstudianteId",
                principalTable: "Estudiantes",
                principalColumn: "EstudianteId",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EstudianteCursos_Cursos_CursoId",
                table: "EstudianteCursos");

            migrationBuilder.DropForeignKey(
                name: "FK_EstudianteCursos_Estudiantes_EstudianteId",
                table: "EstudianteCursos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EstudianteCursos",
                table: "EstudianteCursos");

            migrationBuilder.RenameTable(
                name: "EstudianteCursos",
                newName: "EstudianteCurso");

            migrationBuilder.RenameIndex(
                name: "IX_EstudianteCursos_CursoId",
                table: "EstudianteCurso",
                newName: "IX_EstudianteCurso_CursoId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EstudianteCurso",
                table: "EstudianteCurso",
                columns: new[] { "EstudianteId", "CursoId" });

            migrationBuilder.AddForeignKey(
                name: "FK_EstudianteCurso_Cursos_CursoId",
                table: "EstudianteCurso",
                column: "CursoId",
                principalTable: "Cursos",
                principalColumn: "CursoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EstudianteCurso_Estudiantes_EstudianteId",
                table: "EstudianteCurso",
                column: "EstudianteId",
                principalTable: "Estudiantes",
                principalColumn: "EstudianteId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
