﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class turnoestudiante : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Turno",
                table: "Estudiantes",
                newName: "TurnoId");

            migrationBuilder.CreateIndex(
                name: "IX_Estudiantes_TurnoId",
                table: "Estudiantes",
                column: "TurnoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Estudiantes_Turnos_TurnoId",
                table: "Estudiantes",
                column: "TurnoId",
                principalTable: "Turnos",
                principalColumn: "TurnoId",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Estudiantes_Turnos_TurnoId",
                table: "Estudiantes");

            migrationBuilder.DropIndex(
                name: "IX_Estudiantes_TurnoId",
                table: "Estudiantes");

            migrationBuilder.RenameColumn(
                name: "TurnoId",
                table: "Estudiantes",
                newName: "Turno");
        }
    }
}
