﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace R3App.Server.Migrations
{
    /// <inheritdoc />
    public partial class registro_20231031_10 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CodigoAsistencia",
                table: "RegistroAsistencias",
                newName: "EstadoAsistencia");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EstadoAsistencia",
                table: "RegistroAsistencias",
                newName: "CodigoAsistencia");
        }
    }
}
