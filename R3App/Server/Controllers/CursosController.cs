﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using R3App.Server.Helpers;
using R3App.Shared.DTOs;
using R3App.Shared.Entidades;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;

namespace R3App.Server.Controllers
{
    [ApiController]
    [Route("api/cursos")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class CursosController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IAlmacenadorArchivos almacenadorArchivos;
        private readonly IMapper mapper;
        private readonly string contenedor = "personas";

        public CursosController(ApplicationDbContext context,
            IAlmacenadorArchivos almacenadorArchivos,
            IMapper mapper)
        {
            this.context = context;
            this.almacenadorArchivos = almacenadorArchivos;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Curso>>> Get(
            [FromQuery] PaginacionDTO paginacion)
        {
            var queryable = context.Cursos.AsQueryable();
            await HttpContext
                .InsertarParametrosPaginacionEnRespuesta(queryable, paginacion.CantidadRegistros);
            var cursos = await queryable.OrderBy(x => x.Anio).Paginar(paginacion).ToListAsync();
            return cursos;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Curso>> Get(int id)
        {
            Console.WriteLine("Get(int id)");
            var curso = await context.Cursos
                .Include(x => x.Estudiantes)
                .ThenInclude(e => e.Estudiante)
                .Include(x => x.Asignaturas)
                .ThenInclude(a => a.Asignatura)
                .FirstOrDefaultAsync(curso => curso.CursoId == id);

            if (curso is null)
            {
                return NotFound();
            }

            return curso;
        }

        [HttpGet("buscar/{textoBusqueda}")]
        public async Task<ActionResult<List<Curso>>> Get(string textoBusqueda)
        {
            if (string.IsNullOrWhiteSpace(textoBusqueda))
            {
                return new List<Curso>();
            }

            textoBusqueda = textoBusqueda.ToLower();

            return await context.Cursos
                        .Where(curso => curso.Division.Contains(textoBusqueda))
                        .Take(5)
                        .ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(CursoActualizacionDTO cursoActualizacionDTO)
        {
            var curso = cursoActualizacionDTO.Curso;
            context.Add(curso);
            await context.SaveChangesAsync();
            return curso.CursoId;
        }

        [HttpPut]
        public async Task<ActionResult> Put(CursoActualizacionDTO cursoActualizacionDTO)
        {
            var curso = cursoActualizacionDTO.Curso;

            var cursoDB = await context.Cursos.FirstOrDefaultAsync(a => a.CursoId == curso.CursoId);

            if (cursoDB is null)
            {
                return NotFound();
            }

            cursoDB = mapper.Map(curso, cursoDB);

            cursoActualizacionDTO.AsignaturasIds.ForEach(x =>
            {
                context.Add(new AsignaturaCurso() { AsignaturaId = x, CursoId = curso.CursoId });
            });

            await context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var curso = await context.Cursos.FirstOrDefaultAsync(x => x.CursoId == id);

            if (curso is null)
            {
                return NotFound();
            }

            context.Remove(curso);
            await context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet("actualizar/{id:int}")]
        public async Task<ActionResult<CursoActualizacionDTO>> GetActualizar(int id)
        {
            var cursoActionResult = await Get(id);
            if (cursoActionResult.Result is NotFoundResult) { return NotFound(); }

            var curso = cursoActionResult.Value;
            var asignaturaSeleccionadasIds = curso!.Asignaturas.Select(x => x.AsignaturaId).ToList();
            var asignaturasNoSeleccionadas = await context.Asignaturas
                .Where(x => !asignaturaSeleccionadasIds.Contains(x.AsignaturaId))
                .ToListAsync();

            var modelo = new CursoActualizacionDTO();
            modelo.Curso = curso;
            modelo.Turnos = await context.Turnos.ToListAsync();
            modelo.Docentes = await context.Docentes.ToListAsync();
            modelo.AsignaturasNoSeleccionadas = asignaturasNoSeleccionadas;
            modelo.AsignaturasSeleccionadas = curso.Asignaturas.Select(x => x.Asignatura!).ToList();
            modelo.Estudiantes = curso.Estudiantes.Select(x => x.Estudiante!).ToList();
            return modelo;
        }

        [HttpGet("crear")]
        public async Task<ActionResult<CursoActualizacionDTO>> GetCrear()
        {
            var modelo = new CursoActualizacionDTO();
            modelo.Docentes = await context.Docentes.ToListAsync();
            modelo.AsignaturasNoSeleccionadas = await context.Asignaturas.ToListAsync();
            modelo.Turnos = await context.Turnos.ToListAsync();
            return modelo;
        }

        [HttpGet("asistencias")]
        public async Task<ActionResult<CursoControlAsistenciaDTO>> GetAsistencia([FromQuery] ParamFilterControlAsistenciaDTO modelo)
        {

            var result = new CursoControlAsistenciaDTO();

            result.Turnos = await context.Turnos.ToListAsync();

            if (modelo.TurnoId == 0)
                return result;

            result.Cursos = await context
                .Cursos.Where(x => x.TurnoId == modelo.TurnoId).ToListAsync();

            if (modelo.CursoId == 0)
                return result;

            var fecha = DateTime.Parse(modelo.Fecha);
            //busco si ya extiste registro para esa fecha
            var registroExistente = context.RegistroAsistencias
                .Where(x => x.CursoId == modelo.CursoId && x.Fecha.Date == fecha)
                .Include(e => e.Estudiante)
                .ToListAsync();

            if (registroExistente.Result.Count() > 0)
            {
                result.RegistroAsistencias = registroExistente.Result;
                return result;
            }

            //si no cargar asignatura y docentes
           

            var estudiantes = await context
                .EstudianteCursos.Where(x => x.CursoId == modelo.CursoId)
                .Select(a => a.Estudiante!).ToListAsync();

            foreach (var e in estudiantes)
            {
                var registro = new RegistroAsistencia()
                {
                    Fecha = fecha,
                    CursoId = modelo.CursoId,
                    Curso = result.Cursos.FirstOrDefault(x => x.CursoId == modelo.CursoId),
                    EstudianteId = e.EstudianteId,
                    Estudiante = e,
                };
                result.RegistroAsistencias.Add(registro);
            }

            return result;
        }


        [HttpPut("registrarasistencia")]
        public async Task<ActionResult> SetAsistencia(RegistroAsistencia registro)
        {
            
            var registrosExistente = await context.RegistroAsistencias
                .Where( x => x.Fecha == registro.Fecha && 
                x.CursoId == registro.CursoId).FirstOrDefaultAsync();

            if (registrosExistente == null)
            {
                var estudiantes = await context
               .EstudianteCursos.Where(x => x.CursoId == registro.CursoId)
               .Select(a => a.Estudiante!).ToListAsync();

                foreach (var e in estudiantes)
                {
                    var regNuevo = new RegistroAsistencia()
                    {
                        Fecha = registro.Fecha,
                        CursoId = registro.CursoId,
                        EstudianteId = e.EstudianteId,
                    };
                    context.RegistroAsistencias.Add(regNuevo);
                }
            }
            else
            {
                registrosExistente.EstadoAsistencia = registro.EstadoAsistencia;
                registrosExistente.Observacion = registro.Observacion;
            }

            await context.SaveChangesAsync();

            return NoContent();

        }
    }
}
