﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using R3App.Server.Helpers;
using R3App.Shared.DTOs;
using R3App.Shared.Entidades;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;

namespace R3App.Server.Controllers
{
    [ApiController]
    [Route("api/docentes")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class DocentesController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IAlmacenadorArchivos almacenadorArchivos;
        private readonly IMapper mapper;
        private readonly string contenedor = "personas";

        public DocentesController(ApplicationDbContext context,
            IAlmacenadorArchivos almacenadorArchivos,
            IMapper mapper)
        {
            this.context = context;
            this.almacenadorArchivos = almacenadorArchivos;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Docente>>> Get(
            [FromQuery] PaginacionDTO paginacion)
        {
            var queryable = context.Docentes.AsQueryable();
            await HttpContext
                .InsertarParametrosPaginacionEnRespuesta(queryable, paginacion.CantidadRegistros);
            return await queryable.OrderBy(x => x.Nombre).Paginar(paginacion).ToListAsync();
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Docente>> Get(int id)
        {
            Console.WriteLine("Get(int id)");
            var docente = await context.Docentes.FirstOrDefaultAsync(docente => docente.DocenteId == id);

            if (docente is null)
            {
                return NotFound();
            }

            return docente;
        }

        [HttpGet("buscar/{textoBusqueda}")]
        public async Task<ActionResult<List<Docente>>> Get(string textoBusqueda)
        {
            if (string.IsNullOrWhiteSpace(textoBusqueda))
            {
                return new List<Docente>();
            }

            textoBusqueda = textoBusqueda.ToLower();

            return await context.Docentes
                        .Where(actor => actor.Nombre.ToLower().Contains(textoBusqueda))
                        .Take(5)
                        .ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(Docente docente)
        {
            if (!string.IsNullOrWhiteSpace(docente.Foto))
            {
                var fotoDocente = Convert.FromBase64String(docente.Foto);
                docente.Foto = await almacenadorArchivos.GuardarArchivo(fotoDocente, ".jpg", contenedor);
            }

            context.Add(docente);
            await context.SaveChangesAsync();
            return docente.DocenteId;
        }

        [HttpPut]
        public async Task<ActionResult> Put(Docente docente)
        {
            var docenteDB = await context.Docentes.FirstOrDefaultAsync(a => a.DocenteId == docente.DocenteId);

            if (docenteDB is null)
            {
                return NotFound();
            }

            docenteDB = mapper.Map(docente, docenteDB);

            if (!string.IsNullOrWhiteSpace(docente.Foto))
            {
                var fotoActor = Convert.FromBase64String(docente.Foto);
                docenteDB.Foto = await almacenadorArchivos.EditarArchivo(fotoActor, ".jpg",
                    contenedor, docenteDB.Foto!);
            }

            await context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var docente = await context.Docentes.FirstOrDefaultAsync(x => x.DocenteId == id);

            if (docente is null)
            {
                return NotFound();
            }

            context.Remove(docente);
            await context.SaveChangesAsync();

            if (!string.IsNullOrWhiteSpace(docente.Foto))
            {
                await almacenadorArchivos.EliminarArchivo(docente.Foto!, contenedor);
            }

            return NoContent();
        }
    }
}
