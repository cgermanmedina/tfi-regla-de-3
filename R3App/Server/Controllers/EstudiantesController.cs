﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using R3App.Server.Helpers;
using R3App.Shared.DTOs;
using R3App.Shared.Entidades;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;

namespace R3App.Server.Controllers
{
    [ApiController]
    [Route("api/estudiantes")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class EstudiantesController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IAlmacenadorArchivos almacenadorArchivos;
        private readonly IMapper mapper;
        private readonly string contenedor = "personas";

        public EstudiantesController(ApplicationDbContext context,
            IAlmacenadorArchivos almacenadorArchivos,
            IMapper mapper)
        {
            this.context = context;
            this.almacenadorArchivos = almacenadorArchivos;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Estudiante>>> Get([FromQuery] PaginacionDTO paginacion)
        {
            var queryable = context.Estudiantes.AsQueryable();
            await HttpContext
                .InsertarParametrosPaginacionEnRespuesta(queryable, paginacion.CantidadRegistros);
            return await queryable.OrderBy(x => x.Nombre).Paginar(paginacion).ToListAsync();
        }

        [HttpGet("actualizar/{id:int}")]
        public async Task<ActionResult<EstudianteActualizacionDTO>> GetActualizar(int id)
        {
            var estudiante = await context.Estudiantes.FirstOrDefaultAsync(e => e.EstudianteId == id);

            if (estudiante is null)
            {
                return NotFound();
            }

            var estudianteActualizacionDTO = new EstudianteActualizacionDTO();
            estudianteActualizacionDTO.Estudiante = estudiante;
            estudianteActualizacionDTO.Cursos = await context.Cursos.ToListAsync();
            estudianteActualizacionDTO.Turnos = await context.Turnos.ToListAsync();
            var cursadaActual = await context
                .EstudianteCursos.Where(x => x.EstudianteId == estudiante.EstudianteId)
                .OrderByDescending(e => e.AnioCursada)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if(cursadaActual != null)
                estudianteActualizacionDTO.CursadaActual = cursadaActual;
            else
                estudianteActualizacionDTO.CursadaActual = new EstudianteCurso() { EstudianteId = estudiante.EstudianteId, AnioCursada = DateTime.Now.Year };

            return estudianteActualizacionDTO;
        }

        [HttpGet("crear")]
        public async Task<ActionResult<EstudianteActualizacionDTO>> GetCrear()
        {

            var estudianteActualizacionDTO = new EstudianteActualizacionDTO();
            estudianteActualizacionDTO.Cursos = await context.Cursos.ToListAsync();
            estudianteActualizacionDTO.Turnos = await context.Turnos.ToListAsync();

            return estudianteActualizacionDTO;
        }

        [HttpGet("filtrar")]
        //[AllowAnonymous]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

        public async Task<ActionResult<List<Estudiante>>> Get([FromQuery] ParametrosBusquedaEstudiantesDTO modelo)
        {
            var estudianteQueryable = context.Estudiantes.AsQueryable();

            if (!string.IsNullOrWhiteSpace(modelo.Nombre))
            {
                estudianteQueryable = estudianteQueryable
                    .Where(x => x.Nombre.Contains(modelo.Nombre));
            }

            //if (modelo.EnProceso)
            //{
            //    estudianteQueryable = estudianteQueryable.Where(x => x.);
            //}

            //if (modelo.Estrenos)
            //{
            //    var hoy = DateTime.Today;
            //    peliculasQueryable = peliculasQueryable.Where(x => x.Lanzamiento >= hoy);
            //}

            if (modelo.TurnoId != 0)
            {
                estudianteQueryable = estudianteQueryable
                                        .Where(x => x.TurnoId == modelo.TurnoId);
            }

            //if (modelo.MasVotadas)
            //{
            //    peliculasQueryable = peliculasQueryable.OrderByDescending(p =>
            //      p.VotosPeliculas.Average(vp => vp.Voto));
            //}

            await HttpContext.InsertarParametrosPaginacionEnRespuesta(estudianteQueryable,
                    modelo.CantidadRegistros);

            var peliculas = await estudianteQueryable.Paginar(modelo.PaginacionDTO).ToListAsync();
            return peliculas;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<int>> Post(EstudianteActualizacionDTO estudianteActualizacionDTO)
        {
            var estudiante = estudianteActualizacionDTO.Estudiante;

            if (!string.IsNullOrWhiteSpace(estudiante.Foto))
            {
                var fotoEstudiante = Convert.FromBase64String(estudiante.Foto);
                estudiante.Foto = await almacenadorArchivos.GuardarArchivo(fotoEstudiante, ".jpg", contenedor);
            }

            var cursada = estudianteActualizacionDTO.CursadaActual;
            if (cursada.CursoId != 0)
            {
                var cursadaDB = await context
                    .EstudianteCursos
                    .FirstOrDefaultAsync(x => x.EstudianteId == cursada.EstudianteId && x.CursoId == cursada.CursoId);
                if (cursadaDB == null)
                {
                    context.Add(cursada);
                }
                else
                {
                    cursadaDB = mapper.Map(cursada, cursadaDB);
                }
            }

            context.Add(estudiante);
            await context.SaveChangesAsync();
            return estudiante.EstudianteId;
        }

        [HttpPut]
        public async Task<ActionResult> Put(EstudianteActualizacionDTO estudianteActualizacionDTO)
        {
            var estudiante = estudianteActualizacionDTO.Estudiante;
            var estudianteDB = await context.Estudiantes.FirstOrDefaultAsync(a => a.EstudianteId == estudiante.EstudianteId);

            if (estudianteDB is null)
            {
                return NotFound();
            }

            estudianteDB = mapper.Map(estudiante, estudianteDB);

            if (!string.IsNullOrWhiteSpace(estudiante.Foto))
            {
                var fotoActor = Convert.FromBase64String(estudiante.Foto);
                estudianteDB.Foto = await almacenadorArchivos.EditarArchivo(fotoActor, ".jpg",
                    contenedor, estudianteDB.Foto!);
            }

            var cursada = estudianteActualizacionDTO.CursadaActual;
            if (cursada.CursoId != 0)
            {
                var cursadaDB = await context.EstudianteCursos
                    .FirstOrDefaultAsync(x => x.EstudianteId == cursada.EstudianteId && x.AnioCursada == cursada.AnioCursada);
                if (cursadaDB == null)
                {
                    context.Add(cursada);
                }
                else 
                {
                    context.EstudianteCursos.Remove(cursadaDB);
                    context.EstudianteCursos.Add(cursada);
                }
            }

            await context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var actor = await context.Estudiantes.FirstOrDefaultAsync(x => x.EstudianteId == id);

            if (actor is null)
            {
                return NotFound();
            }

            context.Remove(actor);
            await context.SaveChangesAsync();
            await almacenadorArchivos.EliminarArchivo(actor.Foto!, contenedor);

            return NoContent();
        }
    }
}
