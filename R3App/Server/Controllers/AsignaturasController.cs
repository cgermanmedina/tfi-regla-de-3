﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using R3App.Server;
using R3App.Shared.Entidades;
//using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using R3App.Server.Helpers;
using R3App.Shared.DTOs;

namespace R3App.Server.Controllers
{

    [Route("api/asignaturas")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class AsignaturasController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public AsignaturasController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Asignatura>>> Get(
              [FromQuery] PaginacionDTO paginacion)
        {
            var queryable = context.Asignaturas.AsQueryable();
            await HttpContext
                .InsertarParametrosPaginacionEnRespuesta(queryable, paginacion.CantidadRegistros);
            return await queryable.OrderBy(x => x.Nombre).Paginar(paginacion).ToListAsync();
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<Asignatura>> Get(int id)
        {
            var Asignatura = await context.Asignaturas.FirstOrDefaultAsync(Asignatura => Asignatura.AsignaturaId == id);

            if (Asignatura is null)
            {
                return NotFound();
            }

            return Asignatura;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(Asignatura Asignatura)
        {
            context.Add(Asignatura);
            await context.SaveChangesAsync();
            return Asignatura.AsignaturaId;
        }

        [HttpPut]
        public async Task<ActionResult> Put(Asignatura Asignatura)
        {
            context.Update(Asignatura);
            await context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var filasAfectadas = await context.Asignaturas
                                        .Where(x => x.AsignaturaId == id)
                                        .ExecuteDeleteAsync();

            if (filasAfectadas == 0)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}

