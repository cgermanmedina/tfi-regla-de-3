﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using R3App.Server.Helpers;
using R3App.Shared.DTOs;
using R3App.Shared.Entidades;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace R3App.Server.Controllers
{
    [ApiController]
    [Route("api/areas")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class AreasController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public AreasController(ApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Area>>> Get(
            [FromQuery] PaginacionDTO paginacion)
        {
            var queryable = context.Areas.Include( area => area.Coordinador).AsQueryable();
            await HttpContext
                .InsertarParametrosPaginacionEnRespuesta(queryable, paginacion.CantidadRegistros);
            return await queryable.OrderBy(x => x.Nombre).Paginar(paginacion).ToListAsync();
        }

       

        [HttpGet("{id:int}")]
        [AllowAnonymous]
        public async Task<ActionResult<AreaVisualizarDTO>> Get(int id)
        {
            var area = await context.Areas.Where(area => area.AreaId == id)
                .Include(area => area.Asignaturas)
                .FirstOrDefaultAsync();

            if (area is null)
            {
                return NotFound();
            }

            var modelo = new AreaVisualizarDTO();
            modelo.Area = area;
            modelo.Asignaturas = area.Asignaturas;

            return modelo;
        }




        [HttpGet("buscar/{textoBusqueda}")]
        public async Task<ActionResult<List<Area>>> Get(string textoBusqueda)
        {
            if (string.IsNullOrWhiteSpace(textoBusqueda))
            {
                return new List<Area>();
            }

            textoBusqueda = textoBusqueda.ToLower();

            return await context.Areas
                        .Where(actor => actor.Nombre.ToLower().Contains(textoBusqueda))
                        .Take(5)
                        .ToListAsync();
        }

        [HttpGet("actualizar/{id:int}")]
        public async Task<ActionResult<AreaActualizacionDTO>> GetActualizar(int id)
        {
            var areaActionResult = await Get(id);
            if (areaActionResult.Result is NotFoundResult) { return NotFound(); }

            var areaVisualizarDTO = areaActionResult.Value;
            var asignaturaSeleccionadasIds = areaVisualizarDTO!.Asignaturas.Select(x => x.AsignaturaId).ToList();
            var asignaturasNoSeleccionadas = await context.Asignaturas
                .Where(x => !asignaturaSeleccionadasIds.Contains(x.AsignaturaId))
                .ToListAsync();

            var modelo = new AreaActualizacionDTO();
            modelo.Area = areaVisualizarDTO.Area;
            modelo.AsignaturasNoSeleccionadas = asignaturasNoSeleccionadas;
            modelo.AsignaturasSeleccionadas = areaVisualizarDTO.Asignaturas;
            modelo.Coordinadores = await context.Docentes.ToListAsync();
            return modelo;
        }

        [HttpGet("crear")]
        public async Task<ActionResult<AreaActualizacionDTO>> GetCrear()
        {
            var modelo = new AreaActualizacionDTO();
            modelo.AsignaturasNoSeleccionadas = await context.Asignaturas.ToListAsync(); 
            modelo.Coordinadores = await context.Docentes.ToListAsync();
            return modelo;
        }


        [HttpPost]
        public async Task<ActionResult<int>> Post(AreaActualizacionDTO areaActualizacionDTO)
        {
            var area = areaActualizacionDTO.Area;

            context.Add(area);
            await context.SaveChangesAsync();

            var asignaturas = await context.Asignaturas.Where( x => areaActualizacionDTO.AsignaturasIds.Contains(x.AsignaturaId)).ToListAsync();

            asignaturas.ForEach(x => x.AreaId = area.AreaId);

            await context.SaveChangesAsync();

            return area.AreaId;
        }

        [HttpPut]
        public async Task<ActionResult> Put(AreaActualizacionDTO areaActualizacionDTO)
        {
            var area = areaActualizacionDTO.Area;

            var AreaDB = await context.Areas
                .FirstOrDefaultAsync(a => a.AreaId == area.AreaId);

            if (AreaDB is null)
            {
                return NotFound();
            }

            AreaDB = mapper.Map(area, AreaDB);

            var asignaturas = await context.Asignaturas.Where(x => areaActualizacionDTO.AsignaturasIds.Contains(x.AsignaturaId)).ToListAsync();

            asignaturas.ForEach(x => x.AreaId = area.AreaId);

            await context.SaveChangesAsync();

            return NoContent();
        }



        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var area = await context.Areas.FirstOrDefaultAsync(x => x.AreaId == id);

            if (area is null)
            {
                return NotFound();
            }

            context.Remove(area);
            await context.SaveChangesAsync();
            //await almacenadorArchivos.EliminarArchivo(actor.Foto!, contenedor);

            return NoContent();
        }
    }
}
