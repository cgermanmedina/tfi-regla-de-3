﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using R3App.Server;
using R3App.Shared.Entidades;
//using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;

namespace R3App.Server.Controllers
{

    [Route("api/turnos")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class TurnosController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public TurnosController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        //[AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Turno>>> Get()
        {
            return await context.Turnos
                .Include(x => x.ViceDirector)
                .Include(x => x.Director)
                .ToListAsync();
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Turno>> Get(int id)
        {
            var Turno = await context.Turnos
                .Include(x => x.ViceDirector)
                .Include(x => x.Director)
                .FirstOrDefaultAsync(Turno => Turno.TurnoId == id);

            if (Turno is null)
            {
                return NotFound();
            }

            return Turno;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(Turno Turno)
        {
            context.Add(Turno);
            await context.SaveChangesAsync();
            return Turno.TurnoId;
        }

        [HttpPut]
        public async Task<ActionResult> Put(Turno Turno)
        {
            context.Update(Turno);
            await context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var filasAfectadas = await context.Turnos
                                        .Where(x => x.TurnoId == id)
                                        .ExecuteDeleteAsync();

            if (filasAfectadas == 0)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}

