﻿using AutoMapper;
using R3App.Shared.DTOs;
using R3App.Shared.Entidades;

namespace R3App.Server.Helpers
{
    public class AutoMapperProfiles: Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Area, Area>()
                .ForMember( x => x.Coordinador, option => option.Ignore());

            CreateMap<Docente, Docente>()
                .ForMember(x => x.Foto, option => option.Ignore());

            CreateMap<Estudiante, Estudiante>()
                .ForMember(x => x.Foto, option => option.Ignore());

            CreateMap<RegistroAsistencia, RegistroAsistencia>().IgnoreAllPropertiesWithAnInaccessibleSetter();


            //CreateMap<EstudianteCurso, EstudianteCurso>();

            //CreateMap<Area, Estudiante>()

            //CreateMap<VotoPeliculaDTO, VotoPelicula>();
        }
    }
}
