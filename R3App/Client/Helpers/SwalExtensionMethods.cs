﻿using CurrieTechnologies.Razor.SweetAlert2;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography;

namespace R3App.Client.Helpers
{
    public static class SwalExtensionMethods
    {

        public static async Task<bool> Confirmar(this SweetAlertService swal, string mensaje)
        {
            var resultado = await swal.FireAsync(new SweetAlertOptions
            {
                Title = "Confirmación",
                Text = mensaje,
                Icon = SweetAlertIcon.Warning,
                ShowCancelButton = true,
                AllowOutsideClick = false,
            });

            return !string.IsNullOrEmpty(resultado.Value);

        }

        public static async Task<bool> ConfirmarPerderCambios(this SweetAlertService swal)
        {
            var resultado = await swal.FireAsync(new SweetAlertOptions
            {
                Title = "Confirmación",
                Text = "¿Desea abandonar la página y perder los cambios?",
                Icon = SweetAlertIcon.Warning,
                ShowCancelButton = true,
                AllowOutsideClick = false,
            });

            return !string.IsNullOrEmpty(resultado.Value);

        }
                
        //await swal.FireAsync("Información", "Tarea finalizada correctamente", SweetAlertIcon.Success);

        public static async Task<SweetAlertResult> ShowCompletadoOk(this SweetAlertService swal)
        {
            return await swal.FireAsync(new SweetAlertOptions
            {
                Title = "Información",
                Text = "Tarea finalizada correctamente",
                Icon = SweetAlertIcon.Success,
                AllowOutsideClick = false,
            });

        }



    }
}
