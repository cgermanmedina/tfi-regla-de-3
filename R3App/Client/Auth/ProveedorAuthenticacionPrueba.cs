﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;

namespace R3App.Client.Auth
{
    public class ProveedorAuthenticacionPrueba : AuthenticationStateProvider
    {
        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {

            await Task.Delay(3000);

            var anonimo = new ClaimsIdentity();
            var german = new ClaimsIdentity(
                new List<Claim> 
                { 
                    new Claim("edad","999"),
                    new Claim(ClaimTypes.Name, "German"),
                    new Claim(ClaimTypes.Role,"admin")
                },
                authenticationType: "prueba");

            return await Task.FromResult(new AuthenticationState(new ClaimsPrincipal(german)));

        }
    }
}
