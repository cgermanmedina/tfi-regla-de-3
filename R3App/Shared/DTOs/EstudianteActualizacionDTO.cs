﻿using R3App.Shared.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.DTOs
{
    public class EstudianteActualizacionDTO
    {
        public Estudiante Estudiante { get; set; } = new Estudiante();
        public List<Curso> Cursos { get; set; } = new List<Curso>();
        public List<Turno> Turnos { get; set; } = new List<Turno>();
        public EstudianteCurso CursadaActual { get; set; } = new EstudianteCurso() { AnioCursada = DateTime.Now.Year };

    }
}
