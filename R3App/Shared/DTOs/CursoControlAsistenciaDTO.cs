﻿using R3App.Shared.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.DTOs
{
    public class CursoControlAsistenciaDTO
    {
        public List<Turno> Turnos  { get; set; } = new List<Turno>();
        public List<Curso> Cursos { get; set; } = new List<Curso>();
        public List<Asignatura> Asignaturas { get; set; } = new List<Asignatura>();
        public List<Docente> Docentes { get; set; } = new List<Docente>();
        public List<RegistroAsistencia> RegistroAsistencias { get; set; } = new List<RegistroAsistencia>();

    }
}
