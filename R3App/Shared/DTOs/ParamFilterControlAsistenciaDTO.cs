﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.DTOs
{
    public class ParamFilterControlAsistenciaDTO
    {
        public int Pagina { get; set; } = 1;
        public int CantidadRegistros { get; set; } = 10;
        public PaginacionDTO PaginacionDTO
        {
            get
            {
                return new PaginacionDTO { Pagina = Pagina, CantidadRegistros = CantidadRegistros };
            }
        }

        public string Fecha { get; set; } = string.Empty;
        public int TurnoId { get; set; }
        public int CursoId { get; set; }
        public int AsignaturaId { get; set; }
        public int DocenteId { get; set; }

    }
}
