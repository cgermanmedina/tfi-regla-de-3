﻿using R3App.Shared.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.DTOs
{
    public class AreaVisualizarDTO
    {
        public Area Area { get; set; } = null!;
        public List<Asignatura> Asignaturas { get; set; } = new List<Asignatura>();

    }
}
