﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.DTOs
{
    public class ParametrosBusquedaEstudiantesDTO
    {
        public int Pagina { get; set; } = 1;
        public int CantidadRegistros { get; set; } = 10;
        public PaginacionDTO PaginacionDTO
        {
            get
            {
                return new PaginacionDTO { Pagina = Pagina, CantidadRegistros = CantidadRegistros };
            }
        }

        public int TurnoId { get; set; }
        public string? Nombre { get; set; }
        public bool Matriculados { get; set; }
        public bool EnProceso{ get; set; }
    }
}
