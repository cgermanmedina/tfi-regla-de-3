﻿using R3App.Shared.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.DTOs
{
    public class AreaActualizacionDTO
    {
        public Area Area { get; set; } = new Area();
        public List<Asignatura> AsignaturasSeleccionadas { get; set; } = new List<Asignatura>();
        public List<Asignatura> AsignaturasNoSeleccionadas { get; set; } = new List<Asignatura>();
        public List<Docente> Coordinadores { get; set; } = new List<Docente>();
        public List<int> AsignaturasIds { get; set; } = new List<int>();

    }
}
