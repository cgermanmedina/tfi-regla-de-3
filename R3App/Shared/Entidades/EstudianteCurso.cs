﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.Entidades
{
    public class EstudianteCurso
    {
        public int EstudianteId { get; set; }
        [ForeignKey("EstudianteId")]
        public Estudiante? Estudiante { get; set; }
        public int CursoId { get; set; }
        [ForeignKey("CursoId")]
        public Curso? Curso { get; set; }
        public string Observaciones { get; set; } = "";
        public int AnioCursada { get; set; }

    }
}
