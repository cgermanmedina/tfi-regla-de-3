﻿namespace R3App.Shared.Entidades
{
    public class Documento
    {
        public int DocumentoId { get; set; }
        public string RutaArchivo { get; set; }
        public string Descripcion { get; set; }

    }
}