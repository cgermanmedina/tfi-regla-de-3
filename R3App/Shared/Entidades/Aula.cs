﻿namespace R3App.Shared.Entidades
{
    public class Aula
    {
        public int AulaId { get; set; }
        public int Numero { get; set; }
        public int CapacidadAlumnos { get; set; }
        public string Descripcion { get; set; }
        public string Ubicacion { get; set; }

    }
}
