﻿using System.Collections.Generic;

namespace R3App.Shared.Entidades
{
    public class Legajo
    {
        public int LegajoId { get; set; }
        public int EstudianteId { get; set; }
        public Estudiante? Estudiante { get; set; }
        public bool TieneDocumentacionPendiente { get; set; }
        public List<Documento> Documentos { get; set; }
    }
}
