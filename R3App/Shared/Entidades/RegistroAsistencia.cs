﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace R3App.Shared.Entidades
{
    public class RegistroAsistencia
    {
        public DateTime Fecha { get; set; } = DateTime.Now;
        public int CursoId { get; set; }
        [ForeignKey("CursoId")]
        public Curso? Curso { get; set; }
        public int EstudianteId { get; set; }
        [ForeignKey("EstudianteId")]
        public Estudiante? Estudiante { get; set; }
        public EstadoAsistencia EstadoAsistencia { get; set; }
        public string? Observacion { get; set; } = "";
        
    }

    public enum EstadoAsistencia 
    { 
        Ausente, 
        Presente, 
        Tarde,
        [Description("Ausente con Presencia")]
        AusenteConPresencia
        
    }
}
