﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.Entidades
{
    public class Estudiante
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EstudianteId { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public string Apellido { get; set; }
        public string? TipoDocumento { get; set; }
        public long NroDocumento { get; set; }
        public int? LegajoId { get; set; }
        [ForeignKey("LegajoId")]
        public Legajo? Legajo { get; set; }
        public string Correo { get; set; } = "";
        public string Telefono { get; set; } = "";
        public string? Ocupacion { get; set; }
        public DateTime? FechaNacimiento { get; set; } = null;
        public string? Observacion { get; set; } = "";
        public string? Calle { get; set; } = "";
        public string? NroCalle { get; set; } = "";
        public string? Depto { get; set; } = "";
        public string? CP { get; set; } = "";
        public int? TurnoId { get; set; }
        [ForeignKey("TurnoId")]
        public Turno? Turno { get; set; }
        public string? ContactoEmergencia { get; set; } = "";
        public string? Foto { get; set; }
        public List<EstudianteFamiliar> Familiares { get; set; } = new List<EstudianteFamiliar>();

    }
}
