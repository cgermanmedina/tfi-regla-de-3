﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace R3App.Shared.Entidades
{
    public class Area
    {
        public int AreaId { get; set; }
        public string Nombre { get; set; } = "";
        public int? CoordinadorId { get; set; }
        [ForeignKey("CoordinadorId")]
        public Docente? Coordinador { get; set; }
        public List<Asignatura> Asignaturas { get; set; } = new List<Asignatura>();
    }
}
