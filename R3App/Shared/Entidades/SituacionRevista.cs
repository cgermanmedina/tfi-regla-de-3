﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.Entidades
{
    public enum SituacionRevista
    {
        Titular = 1,
        Interino,
        Suplente,
    }
}
