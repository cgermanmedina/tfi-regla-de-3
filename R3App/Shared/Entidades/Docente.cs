﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace R3App.Shared.Entidades
{
    public class Docente
    {
        public int DocenteId { get; set; }
        public string Nombre { get; set; } = "";
        public string Apellido { get; set; } = "";

        [Range(minimum: 10000000000, maximum: 99999999999, ErrorMessage = "El CUIT es de 11 dígitos")]
        public long CUIT { get; set; }

        [Range(minimum: 100000000, maximum: 999999999, ErrorMessage = "La ficha senzal es de 9 dígitos")]
        public long FichaCensal { get; set; } = 0;
        public string Correo { get; set; } = "";
        public string? Foto { get; set; } = "";
        public string Telefono { get; set; } = "";
        public int SituacionDeRevista { get; set; }
        public List<DocenteAsignatura> Asignaturas { get; set; } = new List<DocenteAsignatura>();
    }
}
