﻿namespace R3App.Shared.Entidades
{
    public class Calificacion
    {
        public int CalificacionId { get; set; }
        public int AsignaturaId { get; set; }
        public Asignatura? Asignatura { get; set; }
        public int DocenteId { get; set; }
        public Docente? Docente { get; set; }
        public int EstudianteId { get; set; }
        public Estudiante? Estudiante { get; set; }
        public int Nota { get; set; }
        public int Periodo { get; set; }
        public DateTime Fecha { get; set; }
        public string Observacion { get; set;} = string.Empty;
    }
}
