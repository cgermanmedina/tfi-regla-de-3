﻿using System.Collections.Generic;

namespace R3App.Shared.Entidades
{
    public class DocenteAsignatura
    {
        public int AsignaturaId { get; set; }
        public Asignatura? Asignatura { get; set; }
        public int DocenteId { get; set; }
        public Docente? Docente { get; set; }
    }
}
