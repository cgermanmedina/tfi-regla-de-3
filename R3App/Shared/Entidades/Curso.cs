﻿using System.ComponentModel.DataAnnotations.Schema;

namespace R3App.Shared.Entidades
{
    public class Curso
    {
        public int CursoId { get; set; }
        public int Anio { get; set; }
        public string Division { get; set; } = "";
        public int? AulaId { get; set; }
        [ForeignKey("AulaId")]
        public Aula? Aula { get; set; }
        public int? TutorId { get; set; }
        [ForeignKey("TutorId")]
        public Docente? Tutor { get; set; }
        public int? PreceptorId { get; set; }
        [ForeignKey("PreceptorId")]
        public Docente? Preceptor { get; set; }
        public int? DelegadoId { get; set; }
        [ForeignKey("DelegadoId")]
        public Estudiante? Delegado { get; set; }
        public int TurnoId { get; set; }
        [ForeignKey("TurnoId")]
        public Turno? Turno { get; set; }
        public List<EstudianteCurso> Estudiantes { get; set; } = new List<EstudianteCurso>();
        public List<AsignaturaCurso> Asignaturas { get; set; } = new List<AsignaturaCurso>();

        public override string ToString()
        {
            return $"{Anio}° {Division} (T.{Turno!.Descripcion.Substring(0,1)})";
        }

    }
}
