﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.Entidades
{
    public class Turno
    {
        public int TurnoId { get; set; }
        public string Descripcion { get; set; } = "";
        public int DirectorId { get; set; }
        public Docente? Director { get; set; }
        public int ViceDirectorId { get; set; }
        public Docente? ViceDirector { get; set; }
        public List<Curso>? Cursos { get; set; }


    }
}
