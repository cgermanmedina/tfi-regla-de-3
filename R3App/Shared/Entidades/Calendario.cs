﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.Entidades
{
    public class Calendario
    {
        public int CalendarioId { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public string Descripcion { get; set; }
        public string Titulo { get; set; }

    }
}
