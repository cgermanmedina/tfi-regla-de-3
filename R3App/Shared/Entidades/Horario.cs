﻿using System;

namespace R3App.Shared.Entidades
{
    public class Horario
    {
        public int Hora { get; set; }
        public int Dia { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }
        public int MinutosHora { get; set; }
    }
}
