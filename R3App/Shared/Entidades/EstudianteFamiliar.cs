﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R3App.Shared.Entidades
{
    public class EstudianteFamiliar
    {

        public int EstudianteId { get; set; }
        public Estudiante? Estudiante { get; set; }
        public int FamiliarId { get; set; }
        public Familiar? Familiar { get; set; }
        public string Parentezco { get; set; } = "";
    }
}
