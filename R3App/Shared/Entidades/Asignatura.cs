﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace R3App.Shared.Entidades
{
    public class Asignatura
    {
        public int AsignaturaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50)]
        public string Nombre { get; set; } = "";

        [Required]
        [Range(minimum: 1, maximum: 5, ErrorMessage = "El valor debe estar entre 1 y 5")]
        public int AnioCurso { get; set; }
        public int? AreaId { get; set; }
        public Area? Area { get; set; }
        public List<DocenteAsignatura> Docentes { get; set; } = new List<DocenteAsignatura>();

        [Range(minimum: 1, maximum: 15, ErrorMessage = "El valor debe estar entre 1 y 15")]
        public int CargaHorariaSemanal { get; set; }
    }
}
