﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace R3App.Shared.Entidades
{
    public class AsignaturaCurso
    {
        public int CursoId { get; set; }
        [ForeignKey("CursoId")]
        public Curso? Curso { get; set; }
        public int AsignaturaId { get; set; }
        [ForeignKey("AsignaturaId")]
        public Asignatura? Asignatura { get; set; }
        public int? DocenteId { get; set; }
        [ForeignKey("DocenteId")]
        public Docente? Docente { get; set; }
        public List<Horario> Horarios { get; set; } = new List<Horario>();
    }
}
