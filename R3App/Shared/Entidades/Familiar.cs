﻿namespace R3App.Shared.Entidades
{
    public class Familiar
    {
        public int FamiliarId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string? TipoDocumento { get; set; }
        public string? NroDocumento { get; set; }
        public string? Correo { get; set; }
        public string Telefono { get; set; }
        public List<EstudianteFamiliar> Estudiantes { get; set;} = new List<EstudianteFamiliar>();
    }
}
